# Matériel

* 3 boîtes **ressources**
  * 10 unités argent
  * 100 unités non-renouvelables
  * 30 unités renouvelables
* 1 plateau 
* 6 jetons **indicateurs**
* 4 paquets de cartes **actions**
  * 12 cartes état
  * 12 cartes ONG
  * 12 cartes entreprise
  * 12 cartes citoyen
* 4 paquets de cartes **situation**
  * 11 cartes état
  * 8 cartes ONG
  * 10 cartes entreprise
  * 9 cartes citoyen 
* 2 paquets de cartes **événements**
  * 13 cartes événement aléatoire
  * 14 cartes événement extrême
* 1 carnet de **scénarios**

# Capacités des acteurs

## État

* dépenser 1 unité d'argent pour remonter un indicateur d'un point. Valable pour les indicateurs **éducation/connaissance**, **bien-être**, **croissance**

## ONG

## Entreprise

## Citoyen

# Déroulement

Le jeu se déroule en 20 tours. Chaque tour est composé de 4 phases :

* Évolution des ressources
* Planification
* Action
* Conséquences

## Évolution des ressources

### argent

* les ressources utilisées au tour précédent sont remises dans la boîte
* \+ 3 pour chaque acteur
* \+/\- pour chaque acteur selon la croissance

### ressources non-renouvelables

* \- 2 par point de démographie

### ressources renouvelables

* \+ 10 de régénération par tour
* \- 2 par point de démographie
* \- 1 par point de pollution

## Planification

* chaque acteur mélange son paquet de cartes action et en pioche 6 au hasard
* une discussion s'engage pour planifier les actions qui vont être réalisées

La discussion est très libre. Elle représente le coeur de la simulation et du jeu de rôle. Des principes existants peuvent être invoqués, des transferts d'argents peuvent être faits...

Les cartes actions peuvent être montrées ou cachées, au choix des acteurs.

Finalement, chaque acteur choisit entre **0 et 3 actions** à réaliser

## Action

Les actions sont révélées et leurs effets appliqués

## Conséquences

On fait évoluer les indicateurs. Les indicateurs sont listés dans l'ordre dans lequel ils doivent êtres évalués.

### croissance

* selon les cartes placées dans le tour

### éducation/connaissances

* selon les cartes placées dans le tour

### pollution

* \+ 1 toutes les 2 unités non-renouvelables dépensées dans le tour

### biodiversité

* \- 1 toutes les 3 unités non-renouvelables dépensées dans le tour

### démographie

* \+ 1 si la croissance est positive

### bien-être

* \- 1 pour chaque indicateur en zone verte
* \- 1 pour chaque indicateur en zone rouge

### seuil critique

Pour chaque indicateur ayant atteint son seuil critique, on tire 1 carte **événement extrême**. Pour chaque carte tirée, on applique l'effet à condition que la condition précisée sur la carte soit réalisée.

# Conditions de victoire

## état

* l'indicateur de bien-être n'est jamais passé sous la barre révolution (2) plus de 3 tours

## ONG 

* l'indicateur de biodiversité est au dessus du seuil critique en fin de partie (3)
* l'indicateur de bien-être est supérieur au seuil acceptable (5) en fin de partie

## entreprise

* l'entreprise n'a pas disparue. Si l'entreprise ne reçoit pas d'argent à un tour (et qu'elle n'est pas sauvée par l'état), elle disparaît.

## citoyen

* l'indicateur de bien-être doit être supérieur ou égal à 7
* l'indicateur de bien-être ne doit pas tomber à 0